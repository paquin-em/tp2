/*Créé par Emilie Paquin - PAQE19528909*/
/*Tests pour la fonction --country avec 1, 2 ou 3 options*/
#include <stdio.h>
#include "../src/countryAndRegion.h"
#include "../src/definitions.h"

json_t *root, *data, *code, *name, *common, *capital, *languages, *borders, *value, *iter;
const char *key;
json_error_t erreur;

  
int init_suite1(void) {  
 
   /*Initialisation de racine*/
   root = json_load_file("data/countries.json", 0, &erreur);
   if (!json_is_array(root)) {
      return -1;
   }
   else {
      return 0;
   }

}

/* The suite cleanup function.
* Closes the temporary file used by the tests.
* Returns zero on success, non-zero otherwise.
*/
int clean_suite1(void) {  
   json_decref(root);
   if (json_is_array(root)) {
      return -1;
   }
   else {
      return 0;
   }
}


/*Test qui valide le retour 1, 2 ou 3 pour l'option choisie:
language = 1
capital = 2
border = 3*/
void testONEOPTION1(void){  

if (json_is_array(root)) {
   	/*Initialisation des paramètres*/
		char *var, *var1, *var2, *var3, *var4;
	   	char *tabVar [6] =  {"USA"}; 
		char *tabVar1 [19] = {"--show-languages"};
		char *tabVar2 [17] = {"--show-capital"};
		char *tabVar3 [17] = {"--show-borders"};
		char *tabVar4 [6] = {"tp2"};
		
		unsigned i = 0;
		int longueur = 6;
         	var = malloc(longueur + 1 * sizeof(char *));
         	strcpy(var, tabVar[0]);
	 	var4 = malloc(longueur + 1 *sizeof(char *));
	 	strcpy(var4, tabVar4[0]);
	 	longueur = 17;
	 	var2 = malloc(longueur + 1 * sizeof(char *));
		strcpy (var2, tabVar2[0]);
		var3 = malloc(longueur + 1 * sizeof(char*));
		strcpy(var3, tabVar3[0]);
		longueur = 19;
		var1 = malloc(longueur + 1 * sizeof(char*));
		strcpy(var1, tabVar1[0]);

	for (i = 0; i < json_array_size(root); i++){

            data = json_array_get(root, i);
            code = json_object_get(data, "cca3");

            if (0 == strcmp(json_string_value(code), var)){
                name = json_object_get(data, "name");
                common = json_object_get(name, "common");
                capital = json_object_get(data, "capital");
                languages = json_object_get(data, "languages");
                borders = json_object_get(data, "borders");
	    }
	}
              
     
     /*Tests*/
     CU_ASSERT_EQUAL(1, validateOneArg(var1, key, languages, value, borders, iter, capital, var4));
     CU_ASSERT_EQUAL(2, validateOneArg(var2, key, languages, value, borders, iter, capital, var4));
     CU_ASSERT_EQUAL(3, validateOneArg(var3, key, languages, value, borders, iter, capital, var4));
     /*Libérer les paramètres*/
     free(var1);
     free(var2);
     free(var3);
     free(var4);
     free(var);
   }  	
   
}

/*Test qui valide le retour 12, 13, 21, 23, 31, 32 selon les options choisies:
--show-languages --show-capital = 12
--show-langauges --show-borders = 13
--show-capital --show-languages = 21
--show-capital --show-borders = 23
--show-borders --show-languages = 31
--show-borders --show-capital = 32 */
void testTWOOPTIONS(void){  

if (json_is_array(root)) {
   	/*Initialisation des paramètres*/
		char *var, *var1, *var2, *var3, *var4;
	   	char *tabVar [6] =  {"USA"}; 
		char *tabVar1 [19] = {"--show-languages"};
		char *tabVar2 [17] = {"--show-capital"};
		char *tabVar3 [17] = {"--show-borders"};
		char *tabVar4 [6] = {"tp2"};
		
		unsigned i = 0;
		int longueur = 6;
         	var = malloc(longueur + 1 * sizeof(char *));
         	strcpy(var, tabVar[0]);
	 	var4 = malloc(longueur + 1 *sizeof(char *));
	 	strcpy(var4, tabVar4[0]);
	 	longueur = 17;
	 	var2 = malloc(longueur + 1 * sizeof(char *));
		strcpy (var2, tabVar2[0]);
		var3 = malloc(longueur + 1 * sizeof(char*));
		strcpy(var3, tabVar3[0]);
		longueur = 19;
		var1 = malloc(longueur + 1 * sizeof(char*));
		strcpy(var1, tabVar1[0]);

	for (i = 0; i < json_array_size(root); i++){

            data = json_array_get(root, i);
            code = json_object_get(data, "cca3");

            if (0 == strcmp(json_string_value(code), var)){
                name = json_object_get(data, "name");
                common = json_object_get(name, "common");
                capital = json_object_get(data, "capital");
                languages = json_object_get(data, "languages");
                borders = json_object_get(data, "borders");
	    }
	}
              
     
     /*Tests dans les 6 ordres possibles 12, 13, 21, 23, 31, 32*/
     CU_ASSERT_EQUAL(12, validateTwoArgs(var1, var2, key, languages, value, borders, iter, capital, var4));
     CU_ASSERT_EQUAL(13, validateTwoArgs(var1, var3, key, languages, value, borders, iter, capital, var4));
     CU_ASSERT_EQUAL(21, validateTwoArgs(var2, var1, key, languages, value, borders, iter, capital, var4));
     CU_ASSERT_EQUAL(23, validateTwoArgs(var2, var3, key, languages, value, borders, iter, capital, var4));
     CU_ASSERT_EQUAL(31, validateTwoArgs(var3, var1, key, languages, value, borders, iter, capital, var4));
     CU_ASSERT_EQUAL(32, validateTwoArgs(var3, var2, key, languages, value, borders, iter, capital, var4));
     /*Libérer les paramètres*/
     free(var1);
     free(var2);
     free(var3);
     free(var4);
     free(var);
   }  	
   
}

/*Test qui valide le retour 123, 132, 213, 231, 312, 321* selon les options choisies:
--show-languages --show-capital --show-borders = 123
--show-langauges --show-borders --show-capital = 132
--show-capital --show-languages --show-borders = 213
--show-capital --show-borders --show-languages = 231
--show-borders --show-languages --show-capital = 312
--show-borders --show-capital --show-languages = 321 */
void testTHREEOPTIONS(void){  

if (json_is_array(root)) {
   	/*Initialisation des paramètres*/
		char *var, *var1, *var2, *var3, *var4;
	   	char *tabVar [6] =  {"USA"}; 
		char *tabVar1 [19] = {"--show-languages"};
		char *tabVar2 [17] = {"--show-capital"};
		char *tabVar3 [17] = {"--show-borders"};
		char *tabVar4 [6] = {"tp2"};
		
		unsigned i = 0;
		int longueur = 6;
         	var = malloc(longueur + 1 * sizeof(char *));
         	strcpy(var, tabVar[0]);
	 	var4 = malloc(longueur + 1 *sizeof(char *));
	 	strcpy(var4, tabVar4[0]);
	 	longueur = 17;
	 	var2 = malloc(longueur + 1 * sizeof(char *));
		strcpy (var2, tabVar2[0]);
		var3 = malloc(longueur + 1 * sizeof(char*));
		strcpy(var3, tabVar3[0]);
		longueur = 19;
		var1 = malloc(longueur + 1 * sizeof(char*));
		strcpy(var1, tabVar1[0]);

	for (i = 0; i < json_array_size(root); i++){

            data = json_array_get(root, i);
            code = json_object_get(data, "cca3");

            if (0 == strcmp(json_string_value(code), var)){
                name = json_object_get(data, "name");
                common = json_object_get(name, "common");
                capital = json_object_get(data, "capital");
                languages = json_object_get(data, "languages");
                borders = json_object_get(data, "borders");
	    }
	}
              
     
     /*Tests dans les 6 ordres possibles - 123, 132, 213, 231, 312, 321*/
     CU_ASSERT_EQUAL(123, validateThreeArgs(var1, var2, var3, key, languages, value, borders, iter, capital, var4));
     CU_ASSERT_EQUAL(132, validateThreeArgs(var1, var3, var2, key, languages, value, borders, iter, capital, var4));
     CU_ASSERT_EQUAL(213, validateThreeArgs(var2, var1, var3, key, languages, value, borders, iter, capital, var4));
     CU_ASSERT_EQUAL(231, validateThreeArgs(var2, var3, var1, key, languages, value, borders, iter, capital, var4));
     CU_ASSERT_EQUAL(312, validateThreeArgs(var3, var1, var2, key, languages, value, borders, iter, capital, var4));
     CU_ASSERT_EQUAL(321, validateThreeArgs(var3, var2, var1, key, languages, value, borders, iter, capital, var4));
     /*Libérer les paramètres*/
     free(var1);
     free(var2);
     free(var3);
     free(var4);
     free(var);
   }  	
   
}

/*Test qui valide le retour -1 lorsqu'une option qui n'existe pas est choisie*/
void testONEOPTION2(void){  

if (json_is_array(root)) {
   	/*Initialisation des paramètres*/
		char *var, *var1, *var2, *var3, *var4;
	   	char *tabVar [6] =  {"USA"}; 
		char *tabVar1 [19] = {"--show-potatoes"};
		char *tabVar2 [17] = {"--show-captain"};
		char *tabVar3 [18] = {"--show-boarders"};
		char *tabVar4 [6] = {"tp2"};
		
		unsigned i = 0;
		int longueur = 6;
         	var = malloc(longueur + 1 * sizeof(char *));
         	strcpy(var, tabVar[0]);
	 	var4 = malloc(longueur + 1 *sizeof(char *));
	 	strcpy(var4, tabVar4[0]);
	 	longueur = 17;
	 	var2 = malloc(longueur + 1 * sizeof(char *));
		strcpy (var2, tabVar2[0]);
		longueur = 18;
		var3 = malloc(longueur + 1 * sizeof(char*));
		strcpy(var3, tabVar3[0]);
		longueur = 19;
		var1 = malloc(longueur + 1 * sizeof(char*));
		strcpy(var1, tabVar1[0]);

	for (i = 0; i < json_array_size(root); i++){

            data = json_array_get(root, i);
            code = json_object_get(data, "cca3");

            if (0 == strcmp(json_string_value(code), var)){
                name = json_object_get(data, "name");
                common = json_object_get(name, "common");
                capital = json_object_get(data, "capital");
                languages = json_object_get(data, "languages");
                borders = json_object_get(data, "borders");
	    }
	}
              
     
     /*Tests*/
     CU_ASSERT_EQUAL(-1, validateOneArg(var1, key, languages, value, borders, iter, capital, var4));
     CU_ASSERT_EQUAL(-1, validateOneArg(var2, key, languages, value, borders, iter, capital, var4));
     CU_ASSERT_EQUAL(-1, validateOneArg(var3, key, languages, value, borders, iter, capital, var4));
     /*Libérer les paramètres*/
     free(var1);
     free(var2);
     free(var3);
     free(var4);
     free(var);
   }  	
   
}


 int main( void ) {
   CU_pSuite pSuite = NULL;

   
   if (CUE_SUCCESS != CU_initialize_registry())
      return CU_get_error();

  
   pSuite = CU_add_suite("Suite_1", init_suite1, clean_suite1);
   if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
   }


   if ((NULL == CU_add_test(pSuite, "test de testONEOPTION1()\n", testONEOPTION1)) ||
       (NULL == CU_add_test(pSuite, "test de testTWOOPTIONS()\n", testTWOOPTIONS)) ||
       (NULL == CU_add_test(pSuite, "test de testTHREEOPTIONS()\n", testTHREEOPTIONS))){
      CU_cleanup_registry();
      return CU_get_error();
   }

   
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   CU_cleanup_registry();
   return CU_get_error();
}

