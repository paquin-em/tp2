/*Créé par Lilly-Gabrielle Champagne - CHAL06589118*/
#include <stdio.h>
#include <string.h>
#include <jansson.h>
#include "CUnit/Basic.h"
#include "../src/borders.h"
#include "../src/countryAndRegion.c"
#include "../src/definitions.h"

json_t *root;
json_error_t erreur;
  
int init_suite1(void) {  
 
   /*Initialisation de racine*/
   root = json_load_file("data/countries.json", 0, &erreur);
   if (!json_is_array(root)) {
      return -1;
   }
   else {
      return 0;
   }
}
/* The suite cleanup function.
* Closes the temporary file used by the tests.
* Returns zero on success, non-zero otherwise.
*/
int clean_suite1(void) {  
   json_decref(root);
   if (json_is_array(root)) {
      return -1;
   }
   else {
      return 0;
   }
}
/* Tests fonctionnels de validerFrontieres avec valeurs ayant un format non conventionnel*/
void testVALEURNONFORMATES(void){  
   if (json_is_array(root)) {
   	/*Initialisation des paramètres*/
   	 char * var1, * var2, * var3, * var4, * var5, * var6;
   	 char * tabVar   [6] =  {"USa"}; 
  	 char * tabVar1  [6] =  {"cAn"};
     char * tabVar2  [6] =  {"itA"};
     char * tabVar3  [6] =  {"meX"};
     char * tabVar4  [6] =  {"FrA"};
     char * tabVar5  [6] =  {"CHE"};
     int longueur = 6;
     var1 = malloc(longueur + 1 * sizeof(char *));
	 strcpy(var1, tabVar[0]);
     var2 = malloc(longueur + 1 * sizeof(char *));
	 strcpy(var2, tabVar1[0]);
	 var3 = malloc(longueur + 1 * sizeof(char *));
	 strcpy(var3, tabVar2[0]);
     var4 = malloc(longueur + 1 * sizeof(char *));
   	 strcpy(var4, tabVar3[0]);
	 var5 = malloc(longueur + 1 * sizeof(char *));
	 strcpy(var5, tabVar4[0]);
     var6 = malloc(longueur + 1 * sizeof(char *));
     strcpy(var6, tabVar5[0]);
     /*Tests*/
     CU_ASSERT(0 == bordersValidation(var1, var2, var3, root));
     CU_ASSERT(0 == bordersValidation(var4, var5, var6, root));
     CU_ASSERT(1 == bordersValidation(var3, var5, var6, root));
     /*Libérer les paramètres*/
     free(var1);
     free(var2);
     free(var3);
     free(var4);
     free(var5);
     free(var6);
   }
}

/* Tests fonctionnels de validerFrontieres avec valeurs ayant un format hors limites (nombres de lettre excédant le format permis)*/
void testVALEURSHORSLIMITES(void){  
   if (json_is_array(root)) {
   	/*Initialisation des paramètres*/
   	 char * var1, * var2, * var3, * var4, * var5, * var6;
   	 char * tabHors1[6] = {"uusa"};
     char * tabHors2[6] = {"ccan"}; 
     char * tabHors3[6] = {"iita"};
     char * tabHors4[6] = {"meex"};
     char * tabHors5[6] = {"frra"};
     char * tabHors6[6] = {"cchhe"};
     int longueur = 6;
     var1 = malloc(longueur + 1 * sizeof(char *));
	 strcpy(var1, tabHors1[0]);
	 var2 = malloc(longueur + 1 * sizeof(char *));
	 strcpy(var2, tabHors2[0]);
	 var3 = malloc(longueur + 1 * sizeof(char *));
	 strcpy(var3, tabHors3[0]);
	 var4 = malloc(longueur + 1 * sizeof(char *));
   	 strcpy(var4, tabHors4[0]);
     var5 = malloc(longueur + 1 * sizeof(char *));
	 strcpy(var5, tabHors5[0]);
     var6 = malloc(longueur + 1 * sizeof(char *));
     strcpy(var6, tabHors6[0]);
     /*Tests*/
     CU_ASSERT(0 == bordersValidation(var1, var2, var3, root));
     CU_ASSERT(0 == bordersValidation(var4, var5, var6, root));
     CU_ASSERT(0 == bordersValidation(var3, var5, var6, root));
     /*Libérer les paramètres*/
      free(var1);
      free(var2);
      free(var3);
      free(var4);
      free(var5);
      free(var6);
   }
}

/* Tests non fonctionnels de validerFrontieres avec valeurs anormales (chaine vide et valeurs non initialisee*/
void testVALIDERFONTIERE(void) {  
  if (json_is_array(root)) {
  	/*Tests*/
      CU_ASSERT(0 == bordersValidation("", "", "", root));
      CU_ASSERT(0 == bordersValidation("can", NULL, "usa", root));
      CU_ASSERT(0 == bordersValidation(NULL, NULL, "", root));

   }
}


/* The main() function for setting up and running the tests.
* Returns a CUE_SUCCESS on successful running, another
* CUnit error code on failure.
*/
 int main() {
   CU_pSuite pSuite = NULL;

   
   if (CUE_SUCCESS != CU_initialize_registry())
      return CU_get_error();

  
   pSuite = CU_add_suite("Suite_1", init_suite1, clean_suite1);
   if (NULL == pSuite) {
      CU_cleanup_registry();
      return CU_get_error();
   }


   if ((NULL == CU_add_test(pSuite, "test de testVALEURNONFORMATES()", testVALEURNONFORMATES)) ||
   	   (NULL == CU_add_test(pSuite, "test de VALIDERFONTIERE()", testVALIDERFONTIERE))         ||
   	   (NULL == CU_add_test(pSuite, "test de testVALEURSHORSLIMITES()", testVALEURSHORSLIMITES))
   	   ){
      CU_cleanup_registry();
      return CU_get_error();
   }

   
   CU_basic_set_mode(CU_BRM_VERBOSE);
   CU_basic_run_tests();
   CU_cleanup_registry();
   return CU_get_error();
}

