/*Créé par Bonnaire Benjamin - BONB03049706*/
#include <stdio.h>
#include <string.h>
#include <jansson.h>
#include "../src/definitions.h"
#include "CUnit/Basic.h"

json_t *root;
json_error_t erreur;

int compareArray(char **arr1,char **arr2,int elt){
    int equal =0;
    int i;
    for (i = 0; i < elt; ++i) {
        if(0 != strcmp(arr1[i],arr2[i])){
            equal =1;
        }
    }
    return equal;
}

int init_suite1(void) {
    
    /*Initialisation de racine*/
    root = json_load_file("data/countries.json", 0, &erreur);
    if (!json_is_array(root)) {
        return -1;
    }
    else {
        return 0;
    }
    
}

/* The suite cleanup function.
 * Closes the temporary file used by the tests.
 * Returns zero on success, non-zero otherwise.
 */
int clean_suite1(void) {
    json_decref(root);
    if (json_is_array(root)) {
        return -1;
    }
    else {
        return 0;
    }
}
/* Tests fonctionnels de validerFrontieres avec valeurs ayant un format non conventionnel*/
void testVALEURNONFORMATES(void){
    if (json_is_array(root)) {
        /*Initialisation des paramètres*/
        
        char var   [6] =  {"USa"};
        char var1  [6] =  {"cAn"};
        char var2  [6] =  {"itA"};
        char var3  [6] =  {"meX"};
        char var4  [6] =  {"FrA"};
        char var5  [6] =  {"CHE"};
        char var6  [6] =  {"non"};
        
        /*Tests*/
        CU_ASSERT(0 == validerLangues(var, var2, var3, root));
        CU_ASSERT(0 == validerLangues(var4, var5, var6, root));
        CU_ASSERT(0 == validerLangues(var3, var5, var6, root));
        CU_ASSERT(0 == validerLangues(var, var1, var6, root));
        CU_ASSERT(0 == validerLangues(var3, var5, var6, root));
        
    }
}

/* Tests fonctionnels de validerFrontieres avec valeurs ayant un format hors limites (nombres de lettre excédant le format permis)*/
void testVALEURSHORSLIMITES(void){
    if (json_is_array(root)) {
        /*Initialisation des paramètres*/
        char hors1[6] = {"uusa"};
        char hors2[6] = {"ccan"};
        char hors3[6] = {"iita"};
        char hors4[6] = {"meex"};
        char hors5[6] = {"frra"};
        char hors6[6] = {"cchhe"};
        /*Tests*/
        CU_ASSERT(1 == validerLangues(hors1, hors2, hors3, root));
        CU_ASSERT(1 == validerLangues(hors4, hors5, hors6, root));
        CU_ASSERT(1 == validerLangues(hors3, hors5, hors6, root));
    }
}


/*Test de la méthode Valider Format qui vérifie si les arguments envoyés à la methode sont bons, retourne 0 si bon et 1 si pas bon*/
void testFORMATCHECK(void){
    if (json_is_array(root)) {
        /*Initialisation des paramètres*/
        char *var1[]= {"aaa","bbb","ccc"};
        char *var2[]= {"aa","bbb","ccc"};
        char *var3[]= {"aaa","bb","ccc"};
        char *var4[]= {"aaa","bbb","cc"};
        char *var5[]= {NULL,"bbb","ccc"};
        /*Tests*/
        CU_ASSERT(0 == formatCheck(var1));
        CU_ASSERT(1 == formatCheck(var2));
        CU_ASSERT(1 == formatCheck(var3));
        CU_ASSERT(1 == formatCheck(var4));
        CU_ASSERT(1 == formatCheck(var5));
        
    }
}
/*Test de la méthode motTraite qui met tous les caractéres d'un string en majuscule*/
void testMOTTRAITE(void){
    if (json_is_array(root)) {
        /*Initialisation des paramètres*/
        char *var1= "moT";
        char *var2= "mOT";
        char *var3= "MoT";
        char *var4= "MOT";
        char *var5= "moT";
        char *var6= "";
        char *res1 = "MOT";
        char *res2 = "";
        /*Tests*/
        CU_ASSERT(0 == strcmp(res1,motTraite(var1)));
        CU_ASSERT(0 == strcmp(res1,motTraite(var2)));
        CU_ASSERT(0 == strcmp(res1,motTraite(var3)));
        CU_ASSERT(0 == strcmp(res1,motTraite(var4)));
        CU_ASSERT(0 == strcmp(res1,motTraite(var5)));
        CU_ASSERT(0 == strcmp(res2,motTraite(var6)));
        
    }
}

/*Test de la méthode motTraite qui met tous les caractéres d'un string en majuscule*/
void testCOMPARE(void){
    if (json_is_array(root)) {
        /*Initialisation des paramètres*/
        struct Country lesPays1[] = {{{"Dutch","French","English"},3},{{"Dutch","English","Spanish"},3},{{"Dutch","Spanish","Italian"},3}};
        char *langue[100];
        char *res1[]={"yes","Dutch"};
        
        struct Country lesPays2[] = {{{"Dutch","French","English"},3},{{"Dutch","English","Spanish"},3}};
        char *res2[]={"yes","Dutch","English"};
        
        struct Country lesPays3[] = {{{"Dutch","French","English"},3},{{"Spanish"},1}};
        char *res3[]={"no"};
        /*Tests*/
        CU_ASSERT(0 == compareArray(res1,compare(lesPays1,3,langue),2));
        CU_ASSERT(0 == compareArray(res2,compare(lesPays2,2,langue),3));
        CU_ASSERT(0 == compareArray(res3,compare(lesPays3,2,langue),1));
        CU_ASSERT(1 == compareArray(res3,compare(lesPays2,2,langue),1));
        
    }
}
/* Tests non fonctionnels de validerFrontieres avec valeurs anormales (chaine vide et valeurs non initialisee*/
void testVALIDERLANGUES(void) {
    if (json_is_array(root)) {
        /*Tests*/
        CU_ASSERT(1 == validerLangues("", "", "", root));
        CU_ASSERT(1 == validerLangues("can", NULL, "usa", root));
        CU_ASSERT(1 == validerLangues(NULL, NULL, "", root));
        
    }
}


/* The main() function for setting up and running the tests.
 * Returns a CUE_SUCCESS on successful running, another
 * CUnit error code on failure.
 */
int main(void) {
    CU_pSuite pSuite = NULL;
    
    
    if (CUE_SUCCESS != CU_initialize_registry())
        return CU_get_error();
    
    
    pSuite = CU_add_suite("Suite_1", init_suite1, clean_suite1);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }
    
    
    if ((NULL == CU_add_test(pSuite, "test de testVALEURNONFORMATES()", testVALEURNONFORMATES)) ||
        (NULL == CU_add_test(pSuite, "test de VALIDERLANGUES()", testVALIDERLANGUES))         ||
        (NULL == CU_add_test(pSuite, "test de testVALEURSHORSLIMITES()", testVALEURSHORSLIMITES)) || (NULL == CU_add_test(pSuite, "test de testMOTTRAITE()", testMOTTRAITE))|| (NULL == CU_add_test(pSuite, "test de testFORMATCHECK()", testFORMATCHECK))|| (NULL == CU_add_test(pSuite, "test de testCOMPARE()", testCOMPARE))
        ){
        CU_cleanup_registry();
        return CU_get_error();
    }
    
    
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}


