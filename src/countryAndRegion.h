//
//  countryAndRegion.h
//  
//
//  Created by Bonnaire Benjamin on 17-04-27.
//
//

#ifndef countryAndRegion_h
#define countryAndRegion_h
#include "definitions.h"
#include <string.h>
#include <jansson.h>
#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"

//Methode pour vailder quand 1 seule option est entree
int validateOneArg(char *command1, const char *key, json_t *languages, json_t *value, json_t *borders, json_t *iter, json_t *capital, char * var2);

//Methode pour valider quand 2 options sont entrees
int validateTwoArgs(char *command1, char *command2, const char *key, json_t *languages, json_t *value, json_t *borders, json_t *iter, json_t *capital, char * var2);

//Methode pour valider quand 3 options sont entrees
int validateThreeArgs(char *command1, char *command2, char *command3, const char *key, json_t *languages, json_t *value, json_t *borders, json_t *iter, json_t *capital, char * var2) ;

//Methode pour afficher le language pour le pays en question
void language_func(const char *key, json_t *languages, json_t *value);

//Methode pour afficher les frontieres pour le pays en question
void border_func(json_t *borders, json_t *iter);

//Methode pour afficher la capitale pour le pays en question
void capital_func(json_t *capital);

/*Méthode pour changer le format de minuscule en majuscule*/
int formatMaj(char * variable1, char * variable2, char * variable3);
#endif /* countryAndRegion_h */
