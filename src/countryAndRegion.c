/*Créé par Emilie Paquin - PAQE19528909*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "countryAndRegion.h"


//Quand on appel --country ou --region avec 1 seule option, imprime les(s) resultat(s), retourne un int dependant de l'option choisie
int validateOneArg(char *command1, const char *key, json_t *languages, json_t *value, json_t *borders, json_t *iter, json_t *capital, char * var2){

	int ret;

	if (0 == strcmp(command1, LANGUAGE)){
        	language_func(key, languages, value);
		ret = 1;
        }else if(0 == strcmp(command1, CAPITAL)){
                capital_func(capital);
		ret = 2;
        }else if(0 == strcmp(command1, BORDER)){
                border_func(borders, iter);
		ret = 3;
        }else{
                fprintf(stderr, "%s: Commande invalide.\n", var2);
		ret = -1;
		
        } 

	return ret;
}

//Quand on appel --country ou --region avec 2 options, imprime les(s) resultat(s), retourne un int en fonction de la combinaison d'options choisies
int validateTwoArgs(char *command1, char *command2, const char *key, json_t *languages, json_t *value, json_t *borders, json_t *iter, json_t *capital, char * var2){
	int ret, temp;
	if (0 == strcmp(command1, LANGUAGE)){
        	language_func(key, languages, value);
		ret = 1;
        }else if(0 == strcmp(command1, CAPITAL)){
                capital_func(capital);
		ret = 2;
        }else if(0 == strcmp(command1, BORDER)){
                border_func(borders, iter);
		ret = 3;
        }else{
                fprintf(stderr, "%s: Commande invalide.\n", var2);
		ret = -1;
        }
        if (0 == strcmp(command2, LANGUAGE)){
                language_func(key, languages, value);
		temp = ret * 10;
		ret = temp + 1;
        }else if(0 == strcmp(command2, CAPITAL)){
                capital_func(capital);
		temp = ret * 10;
		ret = temp + 2;
        }else if(0 == strcmp(command2, BORDER)){
                border_func(borders, iter);
		temp = ret * 10;
		ret = temp + 3;
        }else{
                fprintf(stderr, "%s: Commande invalide.\n", var2);
		ret = -1;
        }
	return ret;

}

//Quand on appel --country ou --region avec 3 options, imprime les(s) resultat(s), retourne un int en fonction de la combinaison d'options choisies
int validateThreeArgs(char *command1, char *command2, char *command3, const char *key, json_t *languages, json_t *value, json_t *borders, json_t *iter, json_t *capital, char * var2){
	int ret, temp;
	if (0 == strcmp(command1, LANGUAGE)){
               language_func(key, languages, value);
		ret = 1;
        }else if(0 == strcmp(command1, CAPITAL)){
               capital_func(capital);
		ret = 2;
        }else if(0 == strcmp(command1, BORDER)){
               border_func(borders, iter);
		ret = 3;
        }else{
               fprintf(stderr, "%s: Commande invalide.\n", var2);
		ret = -1;
        }
        if (0 == strcmp(command2, LANGUAGE)){
               language_func(key, languages, value);
		temp = ret * 10;
		ret = temp + 1;
        }else if(0 == strcmp(command2, CAPITAL)){
               capital_func(capital);
		temp = ret * 10;
		ret = temp + 2;
        }else if(0 == strcmp(command2, BORDER)){
               border_func(borders, iter);
		temp = ret * 10;
		ret = temp + 3;
        }else{
                fprintf(stderr, "%s: Commande invalide.\n", var2);
		ret = -1;
        }
        if (0 == strcmp(command3, LANGUAGE)){
                language_func(key, languages, value);
		temp = ret * 10;
		ret = temp + 1;
        }else if(0 == strcmp(command3, CAPITAL)){
                capital_func(capital);
		temp = ret * 10;
		ret = temp + 2;
        }else if(0 == strcmp(command3, BORDER)){
                border_func(borders, iter);
		temp = ret * 10;
		ret = temp + 3;
        }else{
               fprintf(stderr, "%s: Commande invalide.\n", var2);
		ret = -1;
        }
	return ret;
}

//Méthode qui imprime les langues parlees pour le pays en question
void language_func(const char *key, json_t *languages, json_t *value){
    printf("Languages: ");
    json_object_foreach(languages, key, value){
        printf("%s ", json_string_value(value)); 
    }
    printf("\n");
    
}

//Méthode qui retourne les frontieres pour le pays en question
void border_func(json_t *borders, json_t *iter){
    unsigned k;
    printf("Borders: ");
    for (k = 0; k < json_array_size(borders); k++){
        iter = json_array_get(borders, k);
        printf("%s ", json_string_value(iter));
    }
    printf("\n");
    
}

//Méthode qui retourne la capitale pour le pays en question
void capital_func(json_t *capital){
    printf("Capital: %s\n", json_string_value(capital));
}

//Méthode qui formatte l'entree de la region pour le format Xxxxxx peu importe ce qui est entre
void formatRegion(char * variable){
    int TAILLE = 20;
	int premiere[TAILLE];
	unsigned i;
	for(i=0; i<strlen(variable); i++){
             if(i == 0){
               premiere[i]=toupper((int)variable[i]);
               variable[i]= (char)premiere[i];
             }else{
               premiere[i]=tolower((int)variable[i]);
               variable[i]= (char)premiere[i];
            }
        }
return;
}

/*Méthode pour changer le format de minuscule en majuscule*/
int formatMaj(char * variable1, char * variable2, char * variable3){
    int sortie = 0;
    int TAILLE=3;
    int  vari1[TAILLE], vari2[TAILLE], vari3[TAILLE];
    unsigned i;
    for(i=0; i<3; i++){
       vari1[i]=toupper((int)variable1[i]);
       variable1[i] = (char) vari1[i];
       vari2[i]=toupper((int)variable2[i]);
       variable2[i] = (char) vari2[i];
       if(0 != strcmp(variable3, "non")){
          vari3[i]=toupper((int)variable3[i]);
          variable3[i] = (char) vari3[i]; 
           sortie = 1; 
       }
    }
    return sortie;
}


