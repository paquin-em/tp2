#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "definitions.h"
#include <jansson.h>

#define NOM argv[0]


int main(int argc, char *argv[]) {
    unsigned i;
    int longueur = 0, compteur = 5, sortie = 0, TAILLE=3, index = 6, length2 = 0, ret = 4, length;
    json_t *root, *name, *common, *value = NULL, *iter= NULL, *data, *code, *region, *capital, *languages, *borders;
    const char *key;
    json_error_t erreur;
    char *var1, *var2, *var3, *command1, *command2, *command3;
    int premiere[TAILLE];
    root = json_load_file("data/countries.json", 0, &erreur);
    if(!json_is_array(root)){
        fprintf(stderr, "Erreur, racine n'est pas un array \n");
        json_decref(root);
        exit(EXIT_FAILURE);
    }
    
    
    if (argc < 3){
        fprintf(stderr, "%s: Au moins une commande et un pays doivent etre entres \n", NOM);
        exit(EXIT_FAILURE);
    }
    
    if (0 == strcmp(argv[1], COUNTRY)){ 

        length = strlen(argv[2]);
        var1 = malloc(length + 1 * sizeof(char *));
        strcpy(var1, argv[2]); //nom du pays a traiter
        for(i=0; i<3; i++){
            premiere[i]=toupper((int)var1[i]);
            var1[i] = (char) premiere[i];
        }

	length2 = strlen(argv[0]);
        var2 = malloc(length2 + 1 * sizeof(char *));
        strcpy(var2, argv[0]);


        for (i = 0; i < json_array_size(root); i++){

            data = json_array_get(root, i);
            code = json_object_get(data, "cca3");

            if (0 == strcmp(json_string_value(code), var1)){
                name = json_object_get(data, "name");
                common = json_object_get(name, "common");
                capital = json_object_get(data, "capital");
                languages = json_object_get(data, "languages");
                borders = json_object_get(data, "borders");
                printf("Country: %s\n", json_string_value(common));
                printf("Code: %s\n", json_string_value(code));
                if (argc == 3){
		    ret = 0;
                    break;
                }else if (argc == 4){
			length = strlen(argv[3]);
			command1 = (char *) malloc(length + 1);
			strcpy(command1, argv[3]); //nom de la commande a traiter

                	ret = validateOneArg(command1, key, languages, value, borders, iter, capital, var2);
			
			free(command1);
		    
                }else if (argc == 5) {
			length = strlen(argv[3]);
			command1 = (char *) malloc(length + 1);
			strcpy(command1, argv[3]); //nom de la commande a traiter

			length = strlen(argv[4]);
			command2 = (char *) malloc(length + 1);
			strcpy(command2, argv[4]); //nom de la commande a traiter

                     ret = validateTwoArgs(command1, command2, key, languages, value, borders, iter, capital, var2);
			
		     free(command1);
	             free(command2);
                }else if (argc == 6) {
			length = strlen(argv[3]);
			command1 = (char *) malloc(length + 1);
			strcpy(command1, argv[3]); //nom de la commande a traiter

			length = strlen(argv[4]);
			command2 = (char *) malloc(length + 1);
			strcpy(command2, argv[4]); //nom de la commande a traiter

			length = strlen(argv[5]);
			command3 = (char *) malloc(length + 1);
			strcpy(command3, argv[5]); //nom de la commande a traiter

                    ret = validateThreeArgs(command1, command2, command3, key, languages, value, borders, iter, capital, var2);
			
		    free(command1);
		    free(command2);
		    free(command3);
                }
                

		}
        } if (ret == 4){
		fprintf(stderr, "%s: Ce pays n'existe pas.\n", NOM);
        	exit(EXIT_FAILURE);
	}
        
	free(var1);
	free(var2);
        
    }else if (0 == strcmp(argv[1], REGION)) {
        index = argc;
        
        longueur = strlen(argv[2]);
        var1 = (char *) malloc(longueur + 1);
        strcpy(var1, argv[2]); //nom du pays a traiter

	length2 = strlen(argv[0]);
        var2 = malloc(length2 + 1 * sizeof(char *));
        strcpy(var2, argv[0]);

        formatRegion(var1);

        for (i = 0; i < json_array_size(root); i++){
            data = json_array_get(root, i);
            region = json_object_get(data, "region");
            if (0 == strcmp(json_string_value(region), var1)){
                
                name = json_object_get(data, "name");
                common = json_object_get(name, "common");
                capital = json_object_get(data, "capital");
                languages = json_object_get(data, "languages");
                borders = json_object_get(data, "borders");
                code = json_object_get(data, "cca3");
                printf("\n");
                printf("Country: %s\n", json_string_value(common));
                printf("Code: %s\n", json_string_value(code));
                
                if (index == 3){
		    ret = 0;
                    
                }else if (index == 4){
                    
                    length = strlen(argv[3]);
		    command1 = (char *) malloc(length + 1);
		    strcpy(command1, argv[3]); //nom de la commande a traiter

                    ret = validateOneArg(command1, key, languages, value, borders, iter, capital, var2);
		
		free(command1);

                }else if (index == 5) {
                    
              		length = strlen(argv[3]);
			command1 = (char *) malloc(length + 1);
			strcpy(command1, argv[3]); //nom de la commande a traiter

			length = strlen(argv[4]);
			command2 = (char *) malloc(length + 1);
			strcpy(command2, argv[4]); //nom de la commande a traiter

                     ret = validateTwoArgs(command1, command2, key, languages, value, borders, iter, capital, var2);
		     free(command1);
	             free(command2);
                }else if (index == 6) {
                   	length = strlen(argv[3]);
			command1 = (char *) malloc(length + 1);
			strcpy(command1, argv[3]); //nom de la commande a traiter

			length = strlen(argv[4]);
			command2 = (char *) malloc(length + 1);
			strcpy(command2, argv[4]); //nom de la commande a traiter

			length = strlen(argv[5]);
			command3 = (char *) malloc(length + 1);
			strcpy(command3, argv[5]); //nom de la commande a traiter

                    ret = validateThreeArgs(command1, command2, command3, key, languages, value, borders, iter, capital, var2);
		    free(command1);
		    free(command2);
		    free(command3);
                }
   
            
	    } 
          
        } if (ret == 4){
		fprintf(stderr, "%s: Cette region n'existe pas.\n", NOM);
        	exit(EXIT_FAILURE);
	}
        free(var1);
    } else if (0 == strcmp(argv[1], SAMEBORDER)){

        
        if (argc < 4) { // nom programme (0) nom commande (1) noms pays (3, 4, 5) minimum de 4.
            fprintf(stderr, "%s: Au moins deux pays doivent etre entres \n", NOM);
            exit(EXIT_FAILURE);
        } else {
            compteur -= argc;
            longueur = strlen(argv[2]);
            var1 = malloc(longueur + 1 * sizeof(char *));
            strcpy(var1, argv[2]);
            longueur = strlen(argv[3]);
            var2 = malloc(longueur + 1 * sizeof(char *));
            strcpy(var2, argv[3]);
            if(compteur == 0){
                longueur = strlen(argv[4]);
                var3 = malloc(longueur + 1 * sizeof(char *));
                strcpy(var3, argv[4]);
            }else{
                longueur = strlen("non");
                var3 = malloc(longueur + 1 * sizeof(char *));
                strcpy(var3,"non");
            }
        }
        sortie = bordersValidation(var1, var2, var3, root);
        if(sortie == 0){
            printf("no\n");
        }else{
            printf("yes\n");
        }
        free(var1);
        free(var2);
        free(var3);
        
        
    }else if (0 == strcmp(argv[1], SAMELANGUAGE)){

        if (argc < 4) { // nom programme (0) nom commande (1) noms pays (3, 4, 5) minimum de 4.
            fprintf(stderr, "%s: Au moins deux pays doivent etre entres \n", NOM);
            exit(EXIT_FAILURE);
        } else {
            compteur -= argc;
            longueur = strlen(argv[2]);
            var1 = malloc(longueur + 1 * sizeof(char *));
            strcpy(var1, argv[2]);
            longueur = strlen(argv[3]);
            var2 = malloc(longueur + 1 * sizeof(char *));
            strcpy(var2, argv[3]);
            if(compteur == 0){
                longueur = strlen(argv[4]);
                var3 = malloc(longueur + 1 * sizeof(char *));
                strcpy(var3, argv[4]);
            }else{
                longueur = strlen("non");
                var3 = malloc(longueur + 1 * sizeof(char *));
                strcpy(var3,"non");
            }
            
        }
        validerLangues(var1, var2, var3, root);
        free(var1);
        free(var2);
        free(var3);
    } else {
        fprintf(stderr, "%s: Commandes invalides.\n", NOM);
        exit(EXIT_FAILURE);
    }
    json_decref(root);
    return 0;
}




