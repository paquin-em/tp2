/*Créé par Emilie Paquin - PAQE19528909*/
#ifndef HEADER
#define HEADER

#define LANGUAGE "--show-languages"
#define CAPITAL "--show-capital"
#define BORDER "--show-borders"
#define COUNTRY "--country"
#define REGION "--region"
#define SAMEBORDER "--same-borders"
#define SAMELANGUAGE "--same-language"
#include <assert.h>
#include <jansson.h>



/* structure Country
    contient les langues parlées par un pays ainsi que leur nombre
 */
typedef struct Country {
    char *languages[5];
    int count;
} pays;
//lorsque same--language est choisie
/*Methode vaiderLangues
 @params : les pays entrés par l'utilisateurs 2 ou 3, et le fichier racine json
 @return : 0 si tous les parametres sont correct, 1 si un ou plus des parametres sont invalides
 */
int validerLangues(char * v1, char * v2, char * v3, json_t *root);
/*Methode disp
 @params : prend un tableau de string et le nombre de langues puis les affiches
 @return : void
 */
void disp(char **languages, int count);
/*Methode comp
 @params : un char* contenant une des llangues d'un pays et une structure d'un autre pays pour lequel on veut vérifier s'il parle la meme langue
 @return : 0 si une langue egale a ete trouvee, 1 si aucune langue ne correspond
 */
int comp(const char* langue1,struct Country c2 );
/*Methode initCountry
 @params : un json_t qui contient les langues d'un pays
 @return : une structure de type Country, qui contient les langues parlees d'un pays ainsi que leur nombre
 */
struct Country initCountry(json_t *langues);
/*Methode formatCheck
 @params : l'ensemble de arguments passé par l'utilisateurs
 @return : 0 si tous les parametres sont correct, 1 si un ou plus des parametres sont invalides
 */
int formatCheck(char** ensemble);
/*Methode compare
 @params : un tableau de tous les pays entres par l'utilisateur, leur nombre, et un tableau a remplir qui contiendra les langues communes des pays s'il y'en a, ou "no" si les pays ne partagent aucune langue
 @return : un tableau de string ayant toutes les langues communes des pays
 */
char** compare(struct Country *countries,int count,char * languages[]);
/*Methode motTraite
 @params : un mot a transformer en majuscule
 @return : le mot  en majuscule
 */
char *motTraite(char *origin);

//losrque --same-borders est choisie
int bordersValidation(char * var1, char * var2, char * var3, json_t *root);

/*lorsque l'option --show-languages est choisie
Affiche la bonne langue*/
void language_func(const char * key, json_t * languages, json_t * value);

/*lorsque l'option --show-borders est choisie
Afficher les frontieres pour le pays choisi*/
void border_func(json_t * borders, json_t * iter);

/*lorsque l'option --show-capital est choisie
Afficher la capital*/
void capital_func(json_t * capital);

/*appel de --country avec 1 seule option
Retourne un int en fonction de quelles options ont ete executees*/
int validateOneArg(char * command1, const char * key, json_t * languages, json_t * value, json_t * borders, json_t * iter, json_t * capital, char * var2);

/*appel de --country avec 2 options
Retourne un int en fonction de quelles options ont ete executees*/
int validateTwoArgs(char * command1, char * command2, const char * key, json_t * languages, json_t * value, json_t * borders, json_t * iter, json_t * capital, char * var2);

/*appel de --country avec 3 options
Retourne un int en fonction de quelles options ont ete executees*/
int validateThreeArgs(char * command1, char * command2, char * command3, const char * key, json_t * languages, json_t * value, json_t * borders, json_t * iter, json_t * capital, char * var2);

//Methode pour modifier le format de l'entree standard pour Region, soit Aaaaaaa
void formatRegion(char * variable);

//Méthode pour modifier le format de l'entrée standard en majuscule au lieu de minuscule
int formatMaj(char * variable1, char * variable2, char * variable3);

#endif // HEADER

