//
//  borders.h
//  
//
//  Created by Bonnaire Benjamin on 17-04-27.
//
//

#ifndef borders_h
#define borders_h

/*Méthode pour vérifier si des pays ont les mêmes frontières*/
int bordersValidation(char * v1, char * v2, char * v3, json_t *root);


#endif /* borders_h */
