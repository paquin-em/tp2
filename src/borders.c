/*Créé par Lilly-Gabrielle Champagne - CHAL06589118*/
#include <jansson.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "borders.h"
#include "countryAndRegion.h"
//#include "borders.c"


/*Méthode pour vérifier si des pays ont les mêmes frontières*/
int bordersValidation(char * v1, char * v2, char * v3, json_t *root){ // a mettre dans son propre fichier
    unsigned i, j=0, k=0, h=0;
    int stop = 0, sortie = 0, mmFrontiere1=0,mmFrontiere2=0, mmFrontiere3=0;
    json_t *data, *code, *border1, *border2, *border3, *pays1, *pays2, *pays3;
    if(NULL != v1 && NULL != v2 && NULL != v3 && 3 == strlen(v1) && 3 == strlen(v2) && 3 == strlen(v3) && v1[0]!='\0' && v2[0]!='\0' && v3[0]!='\0'){
        stop = formatMaj(v1, v2, v3);
        /*récupération de l'array correspondant aux frontières des pays entrés dans la sortie standard*/  
        for (i = 0; i < json_array_size(root); i++){
            data = json_array_get(root, i);
            code = json_object_get(data, "cca3");
            if(0 == strcmp(json_string_value(code), v1)){
                border1 = json_object_get(data, "borders"); 
            }else if(0 == strcmp(json_string_value(code), v2)){
                border2 = json_object_get(data, "borders");
            }else if((stop == 1 && 0 == strcmp(json_string_value(code), v3))){
                border3 = json_object_get(data, "borders");
            } 
        }
        /*Test pour vérifier si les pays ont les mêmes frontières*/
        for(j=0; j<json_array_size(border1); j++){
            pays1 = json_array_get(border1, j);
            if(0 == strcmp(json_string_value(pays1),v2) || 0 == strcmp(json_string_value(pays1),v3)){
                mmFrontiere1+=1;
            }
        }
        for(k=0; k<json_array_size(border2); k++){
            pays2 = json_array_get(border2, k);
            if(0 == strcmp(json_string_value(pays2),v1) || 0 == strcmp(json_string_value(pays2),v3)){            
                mmFrontiere2+=1;
            }
        }
        if(stop == 1){
            for(h=0; h<json_array_size(border3); h++){
                pays3 = json_array_get(border3, h);
                if(0 == strcmp(json_string_value(pays3),v2) || 0 == strcmp(json_string_value(pays3),v1)){
                    mmFrontiere3+=1;
                }
            }       
        }
        if(stop == 1){
            if((mmFrontiere1 != 0 && mmFrontiere2 != 0 && mmFrontiere3) && (mmFrontiere1 == mmFrontiere2) && (mmFrontiere2 == mmFrontiere3)){
               sortie = 1;
            }
        }else{
            if((mmFrontiere1 != 0 && mmFrontiere2 != 0) && (mmFrontiere1 == mmFrontiere2)){
                sortie = 1;
            }
        }
    }
    /*sortie de la réponse oui = 1 non = 0*/
    return sortie;
}

