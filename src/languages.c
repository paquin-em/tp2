/* Fichier languages.c créé par Benjamin Bonnaire le 10/04/2017
    Contient les methodes qui permettent de faire fonctionner l'option --same-languages
 */
#include "definitions.h"
#include <jansson.h>
#include <string.h>
#include <ctype.h>
#define NOMBRE_VARIABLES 3
#define TRUE 0
#define FALSE 1

/*Methode vaiderLangues
    @params : les pays entrés par l'utilisateurs 2 ou 3, et le fichier racine json
    @return : 0 si tous les parametres sont correct, 1 si un ou plus des parametres sont invalides
 */
int validerLangues(char * v1, char * v2, char * v3, json_t *root){
    char * ensemble[] = {v1,v2,v3};
    int count =3;
    int valid = FALSE;
    int index;
    unsigned int i =0;
    int j;
    int found = FALSE;
    
    if(strcmp(v3,"non") == 0){
        count = 2;
    }
    valid = formatCheck(ensemble);
    if (valid == TRUE ) {
        for (index=0; index < count ; index ++){
            char *temp = ensemble[index];
            ensemble[index]= malloc(sizeof(motTraite(ensemble[index])));
            strcpy(ensemble[index],motTraite(temp));
        }
        struct Country countries[count] ;
        for (j = 0; j < count; j++){
            
            for(i = 0; i < json_array_size(root); i++)
            {
                json_t *data;
                data = json_array_get(root,i);
                json_t *cca3, *lang;
                const char *name;
                cca3 = json_object_get(data,"cca3");
                name = json_string_value(cca3);
                lang = json_object_get(data,"languages");
                
                if (strcmp(ensemble[j],name)==0) {
                    if (j>0) {
                        found = TRUE;
                    }
                    countries[j] = initCountry(lang);
                    
                }
            }
        }
        if (found == TRUE) {
            char *languages[100];
            compare(countries, count,languages);
            valid = TRUE;
        } else {
            printf("Aucun Pays de ce nom");
            valid= FALSE;
        }
        for (index = 0; index < count; index++) {
            free(ensemble[index]);
        }
        
    } else {
        printf("Paramètres Invalides %d\n ",valid);
    }
    
    return valid;
}
/*Methode compare
 @params : un tableau de tous les pays entres par l'utilisateur, leur nombre, et un tableau a remplir qui contiendra les langues communes des pays s'il y'en a, ou "no" si les pays ne partagent aucune langue
 @return : un tableau de string ayant toutes les langues communes des pays
 */
char** compare(struct Country *countries,int count,char *languages[]){
    struct Country c1 = countries[0];
    char *actual;
    int j,h=0,i,elt=0,used = FALSE,equal = FALSE ;
    for (i = 0; i < c1.count; ++i) {
        actual = malloc(sizeof(c1.languages[i]));
        strcpy (actual,c1.languages[i]);
        for (j = 1; j < count; ++j) {
            equal = comp(actual,countries[j]);
            if (equal == FALSE || (used == TRUE && equal == TRUE)) {
                break;
            }
        }
        
        if (equal == TRUE && used == FALSE){
            char yes[5] = {"yes"};
            used = TRUE;
            languages[h]=(char*)malloc(sizeof(yes));
            strcpy(languages[h],yes);
            elt ++;
            h++;
        }
        if (equal == TRUE && used == TRUE) {
            (languages)[h]=(char *)malloc(sizeof(actual));
            strcpy((languages)[h],actual);
            elt ++;
            h++;
        } else if (equal == FALSE  && used == FALSE){
            char no[3] = {"no"};
            used = TRUE;
            (languages)[h]=(char *)malloc(sizeof(no));
            strcpy(languages[h],no);
            elt ++;
        }
        free(actual);
    }
    printf("\n");
    disp(languages,elt);
    return languages;
}

/*Methode initCountry
 @params : un json_t qui contient les langues d'un pays
 @return : une structure de type Country, qui contient les langues parlees d'un pays ainsi que leur nombre
 */
struct Country initCountry(json_t *langues){
    struct Country new ;
    int i =0;
    int count=0;
    const char *key;
    json_t *value;
    
    json_object_foreach(langues, key, value){
        ++count;
    }
    new.count = count;
    json_object_foreach(langues, key, value){
        new.languages[i] =(char*)malloc(sizeof(json_string_value(value)));
        strcpy(new.languages[i],json_string_value(value));
        ++i;
    }
    
    return new;
}
/*Methode comp
 @params : un char* contenant une des llangues d'un pays et une structure d'un autre pays pour lequel on veut vérifier s'il parle la meme langue
 @return : 0 si une langue egale a ete trouvee, 1 si aucune langue ne correspond
 */

int comp(const char* langue1,struct Country c2 ){
    int equal= FALSE;
    int i;
    for (i = 0; i<c2.count;++i){
        if(strcmp(langue1,c2.languages[i])==TRUE){
            equal = TRUE;
        }
    }
    return equal;
}
/*Methode formatCheck
 @params : l'ensemble de arguments passé par l'utilisateurs
 @return : 0 si tous les parametres sont correct, 1 si un ou plus des parametres sont invalides
 */
int formatCheck(char** ensemble){
    int res = TRUE;
    int i;
    for (i = 0; i< NOMBRE_VARIABLES; ++i) {
        if (ensemble[i]==NULL || strlen(ensemble[i])!=3  ) {
            res = FALSE;
        }
    }
    return res;
}
/*Methode motTraite
 @params : un mot a transformer en majuscule
 @return : le mot  en majuscule
 */
char *motTraite(char *origin){
    int c;
    char *newWord = (char *)malloc(sizeof((origin)));
    for(c=0; origin[c] != '\0'; c++){
        if(islower(origin[c])){
            char new = toupper(origin[c]);
            newWord[c] = (char)new;
        } else {
            newWord[c] = origin[c];
        }
    }
    newWord[c] = '\0';
    return newWord;
}
/*Methode disp
 @params : prend un tableau de string et le nombre de langues puis les affiches
 @return : void
 */
void disp(char **languages,int count){
    int i;
    
    for (i=0; i < count; ++i) {
        printf("%s ",languages[i]);
    
    }
    printf("\n");
}
