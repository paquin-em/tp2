CC=gcc
CFLAGS=-W -Wall
LDFLAGS= -ljansson -lcunit
EXEC=tp2
TESTBORDERS= borders_test
TESTCOUNTRY= countryAndRegion_test
TESTLANGUES = languages_test 
TARG= test/languages_test.o test/countryAndRegion_test.o test/borders_test.o
SRC= src/main.c src/borders.c src/languages.c src/countryAndRegion.c
OBJ= $(SRC:.c=.o)

.PHONY:   all 
all: bin/$(EXEC)  $(TESTBORDERS) $(TESTCOUNTRY) $(TESTLANGUES) 

bin/$(EXEC): $(OBJ)
		@$(CC) -o $@ $^ $(LDFLAGS)

main.o: main.c definitions.h
		$(CC) $(OPTIONS) -c src/main.c 

borders.o: borders.c definitions.h		
		$(CC) $(OPTIONS) -c src/borders.c 

languages.o: languages.c definitions.h	
		$(CC) $(OPTIONS) -c src/languages.c 
%.o: %.c
		@$(CC) -o $@ -c $< $(CFLAGS)

test : test1 test2 test3
test1: $(TESTBORDERS) 
		./test/$(TESTBORDERS) 
$(TESTBORDERS):  src/borders.o test/borders_test.o
		gcc -o test/borders_test  src/borders.o test/borders_test.o -ljansson -lcunit

borders_test.o: test/borders_test.c src/borders.h
		$(CC) -c test/borders_test.c
test2: $(TESTCOUNTRY) 
		./test/$(TESTCOUNTRY) 
$(TESTCOUNTRY): src/countryAndRegion.o test/countryAndRegion_test.o
		gcc -o test/countryAndRegion_test src/countryAndRegion.o test/countryAndRegion_test.o -ljansson -lcunit

countryAndRegion_test.o: test/countryAndRegion_test.c src/countryAndRegion.h
		$(CC) -c test/countryAndRegion_test.c
test3: $(TESTLANGUES)
		./test/$(TESTLANGUES)
$(TESTLANGUES):  src/languages.o test/languages_test.o 
		gcc -o test/languages_test  src/languages.o test/languages_test.o -ljansson -lcunit

languages_test.o: test/languages_test.c src/definitions.h
		$(CC) -c test/languages_test.c
cleantst: rm -fr $(TESTLANGUES) $(TESTBORDERS)$(TESTCOUNTRY) 

clean:
		rm -fr *.o $(OBJ)
		rm -fr *.o $(TARG)
		